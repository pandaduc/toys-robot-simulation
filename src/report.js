import robot from './robot';

var Report = {
    message: [],
    
    /**
     * Log message to console and update Place forms values
     * @param  {object} coordinate
     */
    log(coordinate) {
        this.message.length = 0;
        
        if(coordinate) {
            Object.getOwnPropertyNames(coordinate).forEach((val) => {
                this.message.push(`${val} -> ${coordinate[val]}`); 
                
                this.html(val, coordinate[val]);
            });
            
            console.log( 'Robot is at: ' + this.message.join(', ') );
        }
    },
    
    /**
     * Log error message
     * @param  {string} errors
     */
    error(errors) {
        robot.resetCoordinate();
        
        let message = 'ERROR: ' + errors;
        console.log(message);
        alert(message);
    },
    
    
    /**
     * Assign value to the Place forms fields
     * @param  {string} id
     * @param  {any} value
     */
    html(id, value) {
        if(document.getElementById('place_' + id)) {
            document.getElementById('place_' + id).value = value;    
        }
    }
}

export default Report;