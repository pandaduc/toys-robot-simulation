import robot from './robot';
import Table from './table';
import Report from './report';
import ERROR from './errors';

var Actions = {
    
    /**
     * Execute command
     * @param  {string} command
     */
    execute(command) {
        let splitted_command = command.split(' '); 
        let action_name = splitted_command[0];
        let place_pos = splitted_command[1];
                            
        switch (action_name) {
            case 'MOVE':
                this.move();
                break;
            case 'RIGHT':
                this.right();
                break;
            case 'LEFT':
                this.left();
                break;
            case 'PLACE':
                this.place(place_pos);
                break;
            case 'REPORT':
                this.report();
                break;
            default:
                Report.error(ERROR.INVALID_COMMAND);
                return false;
        }
    },
    
    
    /**
     * Place robot at new position
     * @param  {string} place_pos
     */
    place(place_pos) {
        var newPos = [];
        
        if (place_pos && place_pos.length > 0) {
            newPos = place_pos.split(',');
                            
            if (newPos.length < 3) {
                Report.error(ERROR.INVALID_POSITION);
                return false;
            }
            else {
                let new_coordinate = {
                    x: parseInt(newPos[0]),
                    y: parseInt(newPos[1]),
                    facing: newPos[2].toLowerCase()
                };
                                
                if(Table.isValidCoordinate(new_coordinate)) {
                    robot.setCoordinate(new_coordinate);
                }
                else {
                    Report.error(ERROR.INVALID_INPUT);
                    return false;
                }
            }
        }
        else {
            Report.error(ERROR.INVALID_INPUT);
            return false;
        }
    },
    
    /**
     * Moving forward
     */
    move() {
        var currentCoordinate = robot.getCoordinate();
        var newCoordinate = {
            x: currentCoordinate.x,
            y: currentCoordinate.y,
            facing: currentCoordinate.facing
        }

        var newX, newY;
        
        switch (currentCoordinate.facing) {
            case 'north':
                newY = currentCoordinate.y + 1;
                
                if(Table.isValidXYPos(newY)){
                    newCoordinate.y = newY;
                    robot.setCoordinate(newCoordinate);
                }
                break;
            case 'east':
                newX = currentCoordinate.x + 1;

                if(Table.isValidXYPos(newX)) {
                    newCoordinate.x = newX;
                    robot.setCoordinate(newCoordinate);
                }
                break;
            case 'south':
                newY = currentCoordinate.y - 1;

                if(Table.isValidXYPos(newY)){
                    newCoordinate.y = newY;
                    robot.setCoordinate(newCoordinate);
                }
                break;
            case 'west':
                newX = currentCoordinate.x - 1;

                if(Table.isValidXYPos(newX)){
                    newCoordinate.x = newX;
                    robot.setCoordinate(newCoordinate);
                }
                break;
        }
    },
    
    /**
     * Turn left
     */
    left() {
        this.rotate(false);
    },
    
    /**
     * Turn right
     */
    right() {
        this.rotate(true);
    },
    
    /**
     * @param  {boolean} clockwise  
     */
    rotate(clockwise) {
        var currentFacing = robot.getCoordinate().facing;
        var validDirections = Table.validDirections;
        var direction = (clockwise) ? 1 : -1;
        
        var index = validDirections.indexOf(currentFacing) + direction;

        if (index >= validDirections.length) {
            index = 0;
        }
        else if (index < 0) {
            index = validDirections.length - 1;
        }

        robot.setFacing(validDirections[index]);
    },
    
    /**
     * Report current coordinate
     */
    report() {
        Report.log(robot.getCoordinate());  
    }
}

export default Actions;