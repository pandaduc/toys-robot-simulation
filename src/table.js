var Table = {

    tableEle: document.getElementById('table'),
    arrowEle: null,

    svgns: 'http://www.w3.org/2000/svg',
    config: {
        grid_size: 90,
        offset: 0,
        size: 5
    },
    
    validDirections: ['north', 'east', 'south', 'west'],

    init(config) {
        if (config) this.setConfig(config);

        this.renderGrid();
        this.addRobot();
    },
    
    /**
     * Render grid table
     */
    renderGrid() {
        let config = this.getConfig();
        let size = config.size;
        let offset = config.offset;
        let grid_size = config.grid_size;
        let grid_color = '#9E71E4';
        let tableEle = this.tableEle;

        for (let x = 0; x < size; x++) {
            for (let y = 0; y < size; y++) {
                let rect = document.createElementNS(this.svgns, 'rect');

                rect.setAttributeNS(null, 'x', x * (grid_size + offset));
                rect.setAttributeNS(null, 'y', y * (grid_size + offset));
                rect.setAttributeNS(null, 'height', grid_size);
                rect.setAttributeNS(null, 'width', grid_size);
                rect.setAttributeNS(null, 'fill', grid_color);

                this.tableEle.appendChild(rect);
            }
        }
    },
    
    /**
     * Add robot and arrow on the table
     * Using SVG to create shape
     */
    addRobot() {
        let arrowPoints = '34,58 45,28 56,58';
        let arrowFillColor = '#FFFFFF';
        
        let grid_color = '#9E71E4';
        let grid_size = this.getConfig().grid_size;

        var arrowContainer = document.createElementNS(this.svgns, "g");
        arrowContainer.setAttributeNS(null, 'height', grid_size);
        arrowContainer.setAttributeNS(null, 'width', grid_size);
        this.setArrow(arrowContainer);

        var rect = document.createElementNS(this.svgns, "rect");
        rect.setAttributeNS(null, 'height', grid_size);
        rect.setAttributeNS(null, 'width', grid_size);
        rect.setAttributeNS(null, 'fill', grid_color);
        arrowContainer.appendChild(rect);

        var arrow = document.createElementNS(this.svgns, "polygon");
        arrow.setAttributeNS(null, 'points', arrowPoints);
        arrow.setAttributeNS(null, "fill", arrowFillColor);
        arrowContainer.appendChild(arrow);

        // transform robot: ROTATE, POSITIONING
        this.transformRobot();

        this.tableEle.appendChild(arrowContainer);
    },
    
    /**
     * Transform robot based on coordinate
     * Positiong : x, y
     * Rotate: facing
     */
    transformRobot(coordinate) {
        let grid_size = this.getConfig().grid_size;
        let size = this.getConfig().size;
        let offset = this.getConfig().offset;

        let position = (coordinate) ?
            coordinate :
            {
                x: 0,
                y: 0,
                facing: 'north'
            };
                    
        if(this.isValidCoordinate(position)) {
            let robotAxisX = position.x * (grid_size + offset);
            let robotAxisY = (size - (position.y + 1)) * (grid_size + offset);
            let rotateDeg = 0; // north 

            switch (position.facing.toLowerCase()) {
                case 'north':
                    rotateDeg = 0;
                    break;
                case 'east':
                    rotateDeg = 90;
                    break;
                case 'south':
                    rotateDeg = 180;
                    break;
                case 'west':
                    rotateDeg = 270;
                    break;
            }

            let transform_value = 'translate(' + robotAxisX + ', ' + robotAxisY + ') rotate(' + rotateDeg + ' '+ grid_size/2 +' '+ grid_size/2 +')';

            this.getArrow()
                .setAttributeNS(null, 'transform', transform_value);
        }
    },
    
    
    /**
     * Get config of table
     * @return  {object} {return this.config;}
     */
    getConfig() {
        return this.config;
    },
    
    /**
     * Set config of table
     * @param {any} config
     */
    setConfig(config) {
        let newConfig = Object.assign({}, this.config, config);
        this.config = newConfig;
    },
    
    /**
     * Get Robot SVG Element 
     * @return  {any} {return this.arrowEle;}
     */
    getArrow: function() {
        return this.arrowEle;
    },
    
    /**
     * Set Robot to created Robot SVG Element 
     * @param  {any}
     */
    setArrow: function(robot) {
        this.arrowEle = robot;
    },
    
    
    /**
     * Validate requested coordinate
     * @param  {object} position
     */
    isValidCoordinate(position) {

        let {x, y, facing} = position;
                                
        if (!(this.isValidXYPos(x) && this.isValidXYPos(y) && this.isValidFacing(facing))) {
            return false;
        }
        
        return true;
    },
    
    /**
     * Validate x or y axis value
     * @param  {number} xy
     */
    isValidXYPos(xy) {
        if(typeof xy !== 'number') 
            return false;
        
        return (0 <= xy && xy < this.getConfig().size);
    },
    
    
    /**
     * Validate facing direction, only accept 'north', 'east', 'south', and 'west'
     * @param  {string} facing
     */
    isValidFacing(facing) {
        if(typeof facing !== 'string') 
            return false;
        
        return this.validDirections.indexOf(facing) !== -1;   
    }
}

export default Table;