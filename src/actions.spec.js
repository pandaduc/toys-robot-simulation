import { assert } from 'chai';

import robot from './robot';
import Actions from './actions';
import Table from './table';
import Report from './report';

describe('Actions', () => {
    var original_coordinate = robot.getCoordinate();
    
    function resetCoordinate() {
        robot.setCoordinate(original_coordinate);
    }
    
    describe('Execute Commands', () => {
        describe('PLACE 2,3,SOUTH', () => {
            beforeEach(function() {
                resetCoordinate();
                
                Actions.execute('PLACE 2,3,SOUTH');
            });
                        
            it('should run place()', () => {               
               expect(robot.getCoordinate()).to.eql({
                   x: 2,
                   y: 3,
                   facing: 'south'
               });
            });
        });
        
        describe('MOVE FORWARD 1 UNIT', () => {
            beforeEach(function() {               
                Actions.execute('MOVE');
            });
                        
            it('should run move()', () => {               
               expect(robot.getCoordinate()).to.eql({
                   x: 2,
                   y: 2,
                   facing: 'south'
               })
            });
        });
        
        describe('TURN RIGHT', () => {
            beforeEach(function() {               
                Actions.execute('RIGHT');
            });
                        
            it('should run rotate() to turn to the right', () => {               
               expect(robot.getCoordinate()).to.eql({
                   x: 2,
                   y: 2,
                   facing: 'west'
               })
            });
        });
        
        describe('TURN LEFT', () => {
            beforeEach(function() {               
                Actions.execute('LEFT');
            });
                        
            it('should run rotate() to turn to the left', () => {               
               expect(robot.getCoordinate()).to.eql({
                   x: 2,
                   y: 2,
                   facing: 'south'
               })
            });
        });
        
        describe('REPORT', () => {
            beforeEach(function() {               
                Actions.execute('PLACE 2,1,SOUTH');
                Actions.move();
            });
                        
            it('Should show report of the current coordinate', () => {               
               Actions.execute('REPORT');
               expect(robot.getCoordinate()).to.eql({
                   x: 2,
                   y: 0,
                   facing: 'south'
               })
            });
        });       
        
        
        describe('Preventing Falling Example', () => {
            beforeEach(() => {
                Actions.execute('PLACE 3,0,SOUTH');
                Actions.execute('MOVE');
                Actions.execute('LEFT');
                Actions.execute('MOVE');
                Actions.execute('MOVE');
                Actions.execute('MOVE');
                Actions.execute('MOVE');
            });
            
            it('should not fall', () => {               
               expect(robot.getCoordinate()).to.eql({
                   x: 4,
                   y: 0,
                   facing: 'east'
               })
            });
        })
    });
});