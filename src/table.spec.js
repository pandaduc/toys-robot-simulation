import { assert } from 'chai';

import Table from './table';
import Actions from './actions';
import robot from './robot';

describe('Table', () => {
    var original_coordinate = robot.getCoordinate();

    function resetCoordinate() {
        robot.setCoordinate(original_coordinate);
    }
    
    describe('Validate Coordinate', () => {
                
        beforeEach(() => {
            resetCoordinate();
        });
        
        it('Position is Valid', () => {
            var new_position = {
                x: 2,
                y: 3,
                facing: 'east'
            }
                
            assert.equal(Table.isValidCoordinate(new_position), true); 
        });
        
        it('When `facing` is `south east` => Invalid', () => {
            var new_position = {
                x: 2,
                y: 3,
                facing: 'south east'
            }            
            
            assert.notEqual(Table.isValidCoordinate(new_position), true);
        });
        
        it('When `x` or `y` is `four` => Invalid', () => {
            var new_position = {
                x: 'four',
                y: 3,
                facing: 'south'
            }            
            assert.notEqual(Table.isValidCoordinate(new_position), true);
        });
        
        it('When `x` or `y` is `5` => Invalid', () => {
            var new_position = {
                x: 5,
                y: 3,
                facing: 'south'
            }
             
            assert.notEqual(Table.isValidCoordinate(new_position), true);
        });
    });

    describe('Validate XY Position', () => {
        beforeEach(() => {
            resetCoordinate();
        });
        
        
        it('When `x` or `y` is `4` => Valid', () => {     
            assert.equal(Table.isValidXYPos(4), true);    
        });
        
        it('When `x` or `y` is `5` => Invalid', () => {     
            assert.notEqual(Table.isValidXYPos(5), true);
        });
        
        it('When `x` or `y` is `Five` => Invalid', () => {     
            assert.notEqual(Table.isValidXYPos('Five'), true);
        });
    });

    describe('Validate Facing value', () => {
        beforeEach(() => {
            resetCoordinate();
        });
        
        it('When `facing` is `south` => Valid', () => {     
            assert.equal(Table.isValidFacing('south'), true);
        });
        
        it('When `facing` is `south east` => Invalid', () => {     
            assert.notEqual(Table.isValidFacing('south east'), true);
        });
        
        it('When `facing` is number => Invalid', () => {     
            assert.notEqual(Table.isValidFacing(5), true);
        });
        
    });
    

});