import { assert } from 'chai';

import robot from './robot';
import Actions from './actions';

describe('Robot', () => {
    
    var original_coordinate = robot.getCoordinate();
    function resetCoordinate() {
        robot.setCoordinate(original_coordinate);
    }
    
    describe('Get coordinate', () => {
        beforeEach(() => {
            resetCoordinate();
        });
        it('get coordinate', () => {
            expect(robot.getCoordinate()).to.eql({
                x: 0,
                y: 0,
                facing: 'north'
            });
        });
        
        it('get current coordinate after set new coordinate', () => {
            robot.setCoordinate({
                x: 2,
                y: 4,
                facing: 'west'
            });
            
            expect(robot.getCoordinate()).to.eql({
                x: 2,
                y: 4,
                facing: 'west'
            });
        });
                     
    });
    
    describe('Set Coordinate', () => {
        beforeEach(() => {
            robot.setCoordinate({
                x: 2,
                y: 3,
                facing: 'west'
            });
        });
                    
        it('new coordinate has been set', () => {
            expect(robot.getCoordinate()).to.eql({
                x: 2,
                y: 3,
                facing: 'west'
            });
        });
    });
        
    describe('Get Coordinate Facing', () => {
        beforeEach(() => {
            robot.setCoordinate(original_coordinate);
        });
        
        it('Should get `north` ', () => {
            assert.equal(robot.getCoordinate().facing, 'north');
        });
        
        it('Should get new facing `west` ', () => {
            robot.setCoordinate({
                x: 0,
                y: 0,
                facing: 'west'
            });
            
            expect(robot.getCoordinate().facing).to.equal('west');
        });         
    });
    
    describe('Set Facing', () => {
        beforeEach(() => {
            robot.setCoordinate(original_coordinate);
            
            robot.setFacing('south');
        });
                    
        it('new facing has been set', () => {
            assert.equal(robot.getCoordinate().facing, 'south');
        });
        
        it('should have new coordinate', () => {
            expect(robot.getCoordinate()).to.eql({
                x: 0,
                y: 0,
                facing: 'south'
            });
        });
    });
    
    
    
});
