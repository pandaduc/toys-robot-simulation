var path = require('path');
var webpack = require('webpack');

module.exports = {
    entry: './src/app.js',
    output: {
        path: path.resolve(__dirname, 'dest'),
        pathInfo: true,
        filename: 'bundle.js',
        publicPath: '/build',
        watch: true
    },
    devServer : {
        contentBase: path.resolve(__dirname, 'dest'),
        stats: {
			modules: false,
			cached: false,
			colors: true,
			chunk: false
		},
        // Enable history API fallback so HTML5 History API based
        // routing works. This is a good default that will come
        // in handy in more complicated setups.
        historyApiFallback: true,
        hot: true,
        inline: true,
        progress: true,
        
        host: process.env.HOST,
        port: process.env.PORT || 3067
    },
    module: {
        loaders: [
            { test: /\.js$/, exclude: /node_modules/, loader: "babel-loader" },
            { test: /\.scss$/, loaders: ["style", "css", "sass"] }
        ]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin()
    ]
};