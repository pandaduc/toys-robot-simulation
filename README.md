# Toys Robot Simulation app

## Getting Started

1. Git clone the project repository
```bash
$ git clone https://pandaduc@bitbucket.org/pandaduc/toys-robot-simulation.git
```

2. Make sure you have latest [NODE & NPM](https://nodejs.org/en/download/) installed in your OS
3. Install [webpack](http://webpack.github.io/docs/tutorials/getting-started/) globally via NPM
```bash
$ npm install -g webpack webpack-dev-server
```
4. Install dependencies locally
```bash
$ npm install
```

## Run the application
```bash
$ npm run build
$ npm start
```
Then go to [http://localhost:3067/](http://localhost:3067/)

## Testing
```bash
$ npm test
```