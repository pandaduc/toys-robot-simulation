import './app.scss'

import robot from './robot';
import Actions from './actions';
import Table from './table';
import Report from './report';


(function() {
    
    // initialize app 
    function init() {
        const table_config = {
            grid_size: 90,
            offset: 2,
            size: 5,
        }
        
        // initialize table to the page
        Table.init(table_config);
        
        // update log and inputs
        Report.log(robot.getCoordinate());
        
        // bind events
        initEvents();

        // all functions
        function initEvents() {
            initControlsEvents();
            
            // observe 'set-coordinates' event
            robot.on("set-coordinates", function() {
                let coordinate = robot.getCoordinate();
                
                // 
                Table.transformRobot(coordinate);
                
                // report
                Report.log(coordinate);
            });
        }
        
        // add event listerns to all button controls and form
        function initControlsEvents() {
            var place = document.getElementById('place'),
                move = document.getElementById('move'),
                left = document.getElementById('left'),
                right = document.getElementById('right')

            move.addEventListener('click', function() {
                Actions.execute('MOVE');
            }, false);

            left.addEventListener('click', function() {
                Actions.execute('LEFT');
            }, false);

            right.addEventListener('click', function() {
                Actions.execute('RIGHT');
            }, false);
            
            console.log(getPlaceCommand());
                      
            place.addEventListener('click', function(e) {
                e.preventDefault();
                
                var command = getPlaceCommand();
                
                Actions.execute(command);
            }, false);
            
            function getPlaceCommand() {
                var placeX = document.getElementById('place_x');
                var placeY = document.getElementById('place_y');
                var placeFacing = document.getElementById('place_facing');
                
                var values = [];
                             
                if(placeX.value && placeY.value && placeFacing) {
                    values.push(placeX.value, placeY.value, placeFacing.value);
                }
                
                return 'PLACE ' + values.join(',');
            }
        }

    }

    init();

})()
