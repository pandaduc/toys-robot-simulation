import Report from './Report';

describe('Report', () => {
    describe('Console log', () => {
        it('#Report.log()', () => {
            let coordinate = {
                x: 2,
                y: 2,
                facing: 'east'
            }
            
            expect(Report.log(coordinate));
        });
    });
    
    describe('Console Errors', () => {
        it('#Report.errors()', () => {
            let message = 'Invalid Command';
            
            expect(Report.error(message));
        });
    });
    
    describe('Should update DOM values', () => {
        it('#Report.html()', () => {
            let coordinate = {
                x: 2,
                y: 2,
                facing: 'east'
            };
            
            Object.getOwnPropertyNames(coordinate).forEach((val) => {
                Report.html(val, coordinate[val]);
            });
        });
    });
});