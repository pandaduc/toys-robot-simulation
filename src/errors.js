const ERROR = {
    'INVALID_INPUT': 'Invalid coordinate',
    'INVALID_POSITION': 'Invalid Position',
    'INVALID_COMMAND': 'Cannot execute this command'
}

export default ERROR;