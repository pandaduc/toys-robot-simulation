
var robot = {
    _coordinate: {
        x: 0,
        y: 0,
        facing: 'north',
    },
    
    /** 
     * Observable object helper  
     **/
    _listeners: {},
    on(eventName, listener) {
        if (!this._listeners[eventName]) this._listeners[eventName] = [];
        this._listeners[eventName].push(listener);
    },
    dispatch(eventName) {
        if (this._listeners[eventName]) {
            for (var i = 0; i < this._listeners[eventName].length; i++) {
                this._listeners[eventName][i](this);
            }
        }
    },
    
    /**
     * Get current coordinate
     * @return  {object} {return this._coordinate;}
     */
    getCoordinate() {
        return this._coordinate;
    },
    
    
    /**
     * Set coordinate and dispatch event
     * @param  {object} coordinate
     */
    setCoordinate(coordinate) {
        this._coordinate = coordinate;
        this.dispatch('set-coordinates');
    },
    
    resetCoordinate() {
        this.setCoordinate(this.getCoordinate());
    },
    
    /**
     * Get Facing
     * @return  {object} {return this._coordinate.facing;}
     */
    getFacing() {
        return this._coordinate.facing;
    },
    
    /**
     * Set Facing: NORTH, WEST, SOUTH, EAST
     * @param  {string} direction
     */
    setFacing(direction) {
        var new_coordinate = this.getCoordinate();
        new_coordinate.facing = direction;
        this.setCoordinate(new_coordinate);
    }
}

export default robot;